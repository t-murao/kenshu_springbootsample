package com.example.demo.utils;

import javax.persistence.EntityNotFoundException;

public class TestUtil {
	public static void throwEntityNotFoundException() throws  EntityNotFoundException{
		throw new EntityNotFoundException("hoge");
	}
}
