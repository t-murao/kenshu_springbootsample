package com.example.demo.utils;

import com.example.demo.domain.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 画面遷移後に前画面での操作の結果を示す
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {

	private String text;
	private String type;
	private Employee employee;

	public enum status {
		// 操作対象のentityが見つからなかった場合
		TARGET_NOT_FOUND
	}

	public Message(String text, String type){
		this.text = text;
		this.type = type;
	}
}
