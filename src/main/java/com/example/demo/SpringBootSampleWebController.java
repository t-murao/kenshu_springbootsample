package com.example.demo;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.form.EmployeeInputForm;
import com.example.demo.form.IndexForm;
import com.example.demo.service.EmployeeService;
import com.example.demo.utils.Message;

@Controller
public class SpringBootSampleWebController {

	@Autowired
	EmployeeService employeeService;

	@ModelAttribute
	EmployeeInputForm setUpEmployeeInputForm() {
		return new EmployeeInputForm();
	}

	@ModelAttribute
	IndexForm setUpIndexForm() {
		return new IndexForm();
	}

	/**
	 * 従業員情報の一覧表示
	 */
	@RequestMapping(value = { "/", "/employees" }, method = RequestMethod.GET)
	public ModelAndView index(
			ModelAndView mv,
			@ModelAttribute("model") ModelMap modelMap) {

		return employeeService.index(mv, modelMap);
	}

	/**
	 * 従業員情報の登録情報入力画面
	 */
	@RequestMapping(value = "/employees/input", method = RequestMethod.GET)
	public ModelAndView getInputForm(ModelAndView mv) {
		mv.setViewName("employees/input");
		mv.addObject("employeeInputForm", new EmployeeInputForm()); // 従業員登録用フォーム
		return mv;
	}

	/**
	 * 従業員情報の登録処理
	 */
	@RequestMapping(value = "/employees/input", method = RequestMethod.POST)
	public ModelAndView postInput(
			ModelAndView mv,
			@ModelAttribute @Validated EmployeeInputForm form,
			BindingResult result,
			RedirectAttributes redirectAttributes) {

		return employeeService.create(mv, form, result, redirectAttributes);
	}

	/**
	 * 従業員情報の論理削除処理
	 */
	@RequestMapping(value = "/employees/edit", params = "softDelete", method = RequestMethod.POST)
	public ModelAndView softDelete(
			ModelAndView mv,
			@ModelAttribute IndexForm form,
			RedirectAttributes redirectAttributes) {

		return employeeService.softDelete(mv, form, redirectAttributes);
	}

	/**
	 * 従業員情報の論理削除処理
	 */
	@RequestMapping(value = "/employees/edit", params = "delete", method = RequestMethod.POST)
	public ModelAndView delete(
			ModelAndView mv,
			@ModelAttribute IndexForm form,
			RedirectAttributes redirectAttributes) {

		return employeeService.delete(mv, form, redirectAttributes);
	}

	/**
	 * 一覧画面から登録情報変更画面へ遷移させる処理
	 */
	@RequestMapping(value = "/employees/edit", params = "update", method = RequestMethod.POST)
	public ModelAndView moveUpdate(
			ModelAndView mv,
			@ModelAttribute IndexForm form) {

		return employeeService.getUpdateTarget(mv, form);
	}

	/**
	 * 論理削除済みの従業員情報を復元する
	 */
	@RequestMapping(value = "/employees/edit", params = "restore", method = RequestMethod.POST)
	public ModelAndView restore(
			ModelAndView mv,
			@ModelAttribute IndexForm form,
			RedirectAttributes redirectAttributes) {

		return employeeService.restore(mv, form, redirectAttributes);
	}

	/**
	 * 登録情報の更新フォーム
	 */
	@RequestMapping(value = "/employees/update", method = RequestMethod.POST)
	public ModelAndView update(
			ModelAndView mv,
			@ModelAttribute @Validated EmployeeInputForm form,
			BindingResult result,
			RedirectAttributes redirectAttributes) {

		return employeeService.update(mv, form, result, redirectAttributes);
	}

	/**
	 * 一覧画面から、条件を指定して検索を行った場合の処理
	 */
	@RequestMapping(value = { "/employees" }, method = RequestMethod.POST)
	public ModelAndView search(
			ModelAndView mv,
			RedirectAttributes redirectAttributes,
			@RequestParam(name = "searchValue", required = false) String searchValue,
			@RequestParam(name = "searchTargetColumn", required = true) String searchTargetColumn) {

		return employeeService.search(mv, redirectAttributes, searchValue, searchTargetColumn);
	}

	/**
	 *
	 */
	@RequestMapping("/employees/deleted")
	public ModelAndView delete(
			ModelAndView mv,
			@ModelAttribute("model") ModelMap modelMap
			) throws EntityNotFoundException{

		return employeeService.softdeletedIndex(mv, modelMap);
	}

	/**
	 *
	 */
	@RequestMapping(value = "test")
	public ModelAndView test(ModelAndView mv) throws EntityNotFoundException {
		mv.setViewName("successTemplate");
		employeeService.test();
		return mv;
	}

	/**
	 * <p>例外処理を行うメソッドです。
	 * <p>controllerに対して {@linkplain EntityNotFoundException} が投げられたときに動作します。
	 * <p>例外発生時にこのメソッドに処理が移り、新しい {@linkplain ModelAndView} を返します。
	 * @param e {@linkplain EntityNotFoundException}
	 * @param handlerMethod {@linkplain HandlerMethod}
	 * @param redirectAttributes {@linkplain RedirectAttributes}
	 * @return ModelAndView
	 */
	@ExceptionHandler(EntityNotFoundException.class)
	//	@ResponseStatus(HttpStatus.CREATED)
	public ModelAndView handleBusinessException(
			EntityNotFoundException e,
			HandlerMethod handlerMethod,
			RedirectAttributes redirectAttributes) {

		ExtendedModelMap modelMap = new ExtendedModelMap();
		String callerMethodName = handlerMethod.getMethod().getName();

		// リダイレクト先に送るメッセージオブジェクトの生成
//		String errMessage = "不正な引数が渡されました(" + callerMethodName + ")。" + e.getMessage();
		String errMessage = "処理を正常に終えることができませんでした。";
		modelMap.addAttribute("message", new Message(errMessage, "error"));

		System.out.println(e.getMessage());
		redirectAttributes.addFlashAttribute("model", modelMap);
		return new ModelAndView("redirect:/employees", modelMap);
	}
}