package com.example.demo.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeInputForm {

	@Id
	private Integer id;

	private String no;

	@NotNull
	@Size(min = 1, max = 32)
	private String name;

	@NotNull
	@Size(min = 1, max = 6)
	private String password;

	public EmployeeInputForm(String no) {
		this.no = no;
	}

	public EmployeeInputForm() {
	}
}
