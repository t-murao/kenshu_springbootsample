package com.example.demo.form;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class IndexForm {

	private Integer target;
}
