use sampledb;

-- /*================================================================*/
-- /*  従業員（employees）テーブル削除                                */
-- /*================================================================*/

DROP TABLE IF EXISTS employees;

-- /*================================================================*/
-- /*  従業員（employees）テーブル作成                                */
-- /*================================================================*/

CREATE TABLE employees (
  id INTEGER(2) NOT NULL AUTO_INCREMENT,
  no VARCHAR(6) NOT NULL,
  name VARCHAR(32),
  password VARCHAR(6) NOT NULL,
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)engine=InnoDB;

/*alter table employees character set utf8mb4;*/

-- /*================================================================*/
-- /*  従業員（employees）テーブル データ挿入                         */
-- /*================================================================*/

INSERT INTO employees (no, name, password) VALUES('H20001','安藤直也','zy0001');
INSERT INTO employees (no, name, password) VALUES('H20002','加西春美','yx0002');
INSERT INTO employees (no, name, password) VALUES('H20003','坂口雅也','xw0003');
INSERT INTO employees (no, name, password) VALUES('H20004','田中泰','wv0004');

COMMIT;